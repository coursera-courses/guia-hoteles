$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    if($('#carouselExampleCaptions'))
        $('#carouselExampleCaptions').carousel({
            interval: 2500,
            keyboard: false
        })
    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('El modal se esta mostrando')
        $('#bottonmodal').removeClass('btn-outline-secondary');
        $('#bottonmodal').addClass('btn-primary');
        $('#bottonmodal').prop('disabled',true);
    })
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('El modal se mostro')
    })
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('El modal se oculta')
    })
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('El modal se oculto')
        $('#bottonmodal').removeClass('btn-primary');
        $('#bottonmodal').addClass('btn-outline-secondary');
        $('#bottonmodal').prop('disabled',false);
    })
})